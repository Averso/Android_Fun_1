package com.franke.myfirstapp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class MyActivity extends AppCompatActivity {

    public final static  String EXTRA_MESSAGE = "com.franke.myfirstapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //after clicking the send button
    //note to myself: musi byc publiczne, void, view jako jedyny parametr
    public void sendMessage(View View)
    {
        //note to myself: abstakcyjny opis operacji do wykonania
        //można użyć do opalania nowej aktywności, wysyłania , rozpoczynania serwisów

        Intent intent = new Intent(this,DisplayMessageActivity.class);
        EditText eT = (EditText) findViewById(R.id.edit_message);

        String message = eT.getText().toString();


        //it's totally without point to do such thing,
        //but it's just example
        //isEmpty it's API 9, we've got set min API=8
        if(Build.VERSION.SDK_INT >= 9)
        {
            if(!message.isEmpty())
            {
                intent.putExtra(EXTRA_MESSAGE,message);
                startActivity(intent);
            }
            else
                Toast.makeText(this,R.string.empty_message_warning,Toast.LENGTH_LONG).show();

        }
        else //lower than 8 API - use equals
        {
            if(!message.equals(""))
            {
                intent.putExtra(EXTRA_MESSAGE,message);
                startActivity(intent);
            }
            else
                Toast.makeText(this,R.string.empty_message_warning,Toast.LENGTH_LONG).show();

        }
    }

}
